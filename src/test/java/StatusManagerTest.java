
import com.training.Indicator;
import com.training.StatusManager;
import org.junit.*;

import static org.junit.Assert.assertEquals;


public class StatusManagerTest {
	
	String expectedOkStr = "Ok";
	String expectedErrorStr = "Error";
	String expectedErrorColorStr = "#FF0000";
	
	StatusManager manager;
	Indicator indicator;
	
	//execute before class
   @BeforeClass
   public static void beforeClass() {
      System.out.println("in before class");
   }

   //execute after class
   @AfterClass
   public static void  afterClass() {
      System.out.println("in after class");
   }
   
   //execute before test
	@Before
	public void setUp(){
		System.out.println("in before every test case");
		indicator = new Indicator();
		manager = new StatusManager(indicator);
	}
	
	//execute after test
   @After
   public void after() {
      System.out.println(" in after every test case");
   }
		
	
	////test case
	@Test
	public void testErrorStatusCode(){
		manager.changeStatusByCode(1);
		assertEquals(expectedErrorStr, manager.getStatus());
		assertEquals( null, indicator.getColor());
		
	}

	@Test
	public void testOkStatusCode(){
		manager.changeStatusByCode(2);
		assertEquals(expectedOkStr, manager.getStatus());
		assertEquals(null, indicator.getColor());
		
	
	}
	
	//test case ignore and will not execute
	@Test
	@Ignore
	public void testIgnore() {
		System.out.println("in ignore test");
	}

}
